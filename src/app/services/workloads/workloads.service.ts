import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Workload } from 'src/app/models/workload';

@Injectable({
  providedIn: 'root'
})
export class WorkloadsService {

  baseUrl: string = environment.API_URL;

  constructor(public httpClient: HttpClient) {

  }

  getWorkloads() {
      return this.httpClient.get(this.baseUrl + '/workload');
  }

  getWorkload(id:number) {
      return this.httpClient.get(this.baseUrl + '/workload/' + id.toString());
  }

  updateWorkload(workload: Workload) {
      return this.httpClient.put(this.baseUrl + '/workload', workload);
  }

  addWorkload(workload: Workload){
    return this.httpClient.post(this.baseUrl + '/workload', workload);
  }

  deleteWorkload(id: number){
    return this.httpClient.delete(this.baseUrl + '/workload/' + id.toString());
  }

}
