import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Volume } from 'src/app/models/volume';

@Injectable({
  providedIn: 'root'
})
export class VolumesService {

  baseUrl: string = environment.API_URL;

  constructor(public httpClient: HttpClient) {

  }

  getVolumes() {
      return this.httpClient.get(this.baseUrl + '/volume');
  }

  getVolume(id:number) {
      return this.httpClient.get(this.baseUrl + '/volume/' + id.toString());
  }

  updateVolume(volume: Volume) {
      return this.httpClient.put(this.baseUrl + '/volume', volume);
  }

  addVolume(volume: Volume){
    return this.httpClient.post(this.baseUrl + '/volume', volume);
  }

  deleteVolume(id: number){
    return this.httpClient.delete(this.baseUrl + '/volume/' + id.toString());
  }

}
