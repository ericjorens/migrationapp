import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Migration } from 'src/app/models/migration';
import { MigrationHistory } from 'src/app/models/migration-history';

@Injectable({
  providedIn: 'root'
})
export class MigrationsService {

  baseUrl: string = environment.API_URL;

  constructor(public httpClient: HttpClient) {

  }

  getMigrations() {
    return this.httpClient.get(this.baseUrl + '/migration');
  }

  getMigration(id: number) {
    return this.httpClient.get(this.baseUrl + '/migration/' + id.toString());
  }

  getMigrationHistory(id: number) {
    return this.httpClient.get(this.baseUrl + '/migration/history/' + id.toString());
  }

  getMigrationHistoryPercentage(migrationHistory: MigrationHistory) {
    return this.httpClient.put(this.baseUrl + '/migration/history/progress', migrationHistory);
  }

  getAllMigrationHistory(id: number) {
    return this.httpClient.get(this.baseUrl + '/migration/history/' + id.toString() + '/all');
  }

  startMigration(id: number) {
    return this.httpClient.get(this.baseUrl + '/migration/' + id.toString() + '/start');
  }

  stopMigration(migrationHistoryId: number) {
    return this.httpClient.get(this.baseUrl + '/migration/' + migrationHistoryId.toString() + '/stop');
  }

  updateMigration(migration: Migration) {
    return this.httpClient.put(this.baseUrl + '/migration', migration);
  }

  addMigration(migration: Migration) {
    return this.httpClient.post(this.baseUrl + '/migration', migration);
  }

  deleteMigration(id: number) {
    return this.httpClient.delete(this.baseUrl + '/migration/' + id.toString());
  }

}
