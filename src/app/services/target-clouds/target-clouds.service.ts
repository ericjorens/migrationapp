import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { TargetCloud } from 'src/app/models/target-cloud';

@Injectable({
  providedIn: 'root'
})
export class TargetCloudsService {

  baseUrl: string = environment.API_URL;

  constructor(public httpClient: HttpClient) {

  }

  getTargetClouds() {
      return this.httpClient.get(this.baseUrl + '/targetcloud');
  }

  getTargetCloud(id:number) {
      return this.httpClient.get(this.baseUrl + '/targetcloud/' + id.toString());
  }

  updateTargetCloud(targetCloud: TargetCloud) {
      return this.httpClient.put(this.baseUrl + '/targetcloud', targetCloud);
  }

  addTargetCloud(targetCloud: TargetCloud){
    return this.httpClient.post(this.baseUrl + '/targetcloud', targetCloud);
  }

  deleteTargetCloud(id: number){
    return this.httpClient.delete(this.baseUrl + '/targetcloud/' + id.toString());
  }

}
