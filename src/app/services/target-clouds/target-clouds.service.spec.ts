import { TestBed } from '@angular/core/testing';

import { TargetCloudsService } from './target-clouds.service';

describe('TargetCloudsService', () => {
  let service: TargetCloudsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TargetCloudsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
