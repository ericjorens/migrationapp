import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Credential } from '../../models/credential';

@Injectable({
  providedIn: 'root'
})
export class CredentialsService {

  baseUrl: string = environment.API_URL;

  constructor(public httpClient: HttpClient) {

  }

  getCredentials() {
      return this.httpClient.get(this.baseUrl + '/credential');
  }

  getCredential(id:number) {
      return this.httpClient.get(this.baseUrl + '/credential/' + id.toString());
  }

  updateCredential(credential: Credential) {
      return this.httpClient.put(this.baseUrl + '/credential', credential);
  }

  addCredential(credential: Credential){
    return this.httpClient.post(this.baseUrl + '/credential', credential);
  }

  deleteCredential(id: number){
    return this.httpClient.delete(this.baseUrl + '/credential/' + id.toString());
  }

}
