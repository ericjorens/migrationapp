import { Component, OnInit } from '@angular/core';
import { CredentialsService } from 'src/app/services/credentials/credentials.service';
import { Credential } from '../../models/credential';
import { ToastrService } from 'ngx-toastr';
import { HttpHeaderResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-credentials',
  templateUrl: './credentials.component.html',
  styleUrls: ['./credentials.component.scss']
})
export class CredentialsComponent implements OnInit {

  public isEditing: Boolean = false;
  public isAdding: Boolean = false;

  public credentials: Array<Credential>;
  public credential: Credential;

  public headers: HttpHeaderResponse;

  constructor(private service: CredentialsService, private toastr: ToastrService) {
    this.fetch();
  }

  ngOnInit(): void {
  }

  fetch() {
    this.service.getCredentials().subscribe((resp:Array<Credential>) => {
        this.toastr.success("Ok!");
        this.credentials = resp;
    }, (err:HttpErrorResponse) => {
      this.toastr.error(err.error.message);
   });
  }

  edit(c: Credential) {
    this.credential = Object.assign(c);
    this.isEditing = true;
  }

  add() {
    this.credential = new Credential();
    this.isAdding = true;
  }

  save() {
    this.service.addCredential(this.credential).subscribe(resp => {
        this.toastr.success("Save Ok!");
        this.fetch();
        this.isAdding = false;
    }, (err:HttpErrorResponse) => {
      this.toastr.error(err.error.message);
   });
  }

  update() {
    this.service.updateCredential(this.credential).subscribe(resp => {
        this.toastr.success("Update Ok!");
        this.fetch();
        this.isEditing = false;
    }, (err:HttpErrorResponse) => {
      this.toastr.error(err.error.message);
   });

  }

  delete() {
    this.service.deleteCredential(this.credential.id).subscribe(resp => {
        this.toastr.success("Delete Ok!");
        this.fetch();
        this.isEditing = false;
    }, (err:HttpErrorResponse) => {
       this.toastr.error(err.error.message);
    });
  }

  goBack() {
    this.fetch();
    this.isAdding = false;
    this.isEditing = false
  }
}
