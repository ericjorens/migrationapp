import { Component, OnInit } from '@angular/core';
import { Workload } from 'src/app/models/workload';
import { WorkloadsService } from 'src/app/services/workloads/workloads.service';
import { CredentialsService } from 'src/app/services/credentials/credentials.service';
import { Volume } from 'src/app/models/volume';
import { Credential } from 'src/app/models/credential';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { VolumesService } from 'src/app/services/volumes/volumes.service';

@Component({
  selector: 'app-workloads',
  templateUrl: './workloads.component.html',
  styleUrls: ['./workloads.component.scss']
})
export class WorkloadsComponent implements OnInit {

  public isEditing: Boolean = false;
  public isAdding: Boolean = false;
  public isOwnerSelect: Boolean = false;
  public isVolumeSelect: Boolean = false;

  public workloads: Array<Workload> = [];
  public workload: Workload;

  public credentials: Array<Credential> = [];

  public volumes: Array<Volume> = [];
  public selectedVolumes: Array<Volume> = [];

  constructor(private volumeService: VolumesService, private credentialService: CredentialsService, private service: WorkloadsService, private toastr: ToastrService) {
    this.fetch();
  }

  ngOnInit(): void {
  }

  fetch() {
    this.service.getWorkloads().subscribe((res: Workload[]) => {
      this.toastr.success("Ok!");
      this.workloads = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  edit(c: Workload) {
    this.workload = Object.assign(c);
    this.isEditing = true;
  }

  add() {
    this.workload = new Workload();
    this.selectedVolumes = Object.assign([]);
    this.isAdding = true;
  }

  save() {
    this.service.addWorkload(this.workload).subscribe((res: any) => {
      this.toastr.success("Save Ok!");
      this.fetch();
      this.isAdding = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  update() {
    this.service.updateWorkload(this.workload).subscribe((res: any) => {
      this.toastr.success("Update Ok!");
      this.fetch();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });

  }

  delete() {
    this.service.deleteWorkload(this.workload.id).subscribe((res: any) => {
      this.toastr.success("Delete Ok!");
      this.fetch();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  getVolumes(w: Workload): string {
    if (!!w) {
      let vols: string = '';
      w.volume.forEach((v: Volume) => {
        vols += v.mountPoint + '    ';
      });
      return vols;
    }
    return '';
  }

  goBack() {
    this.fetch();
    this.isAdding = false;
    this.isEditing = false
  }

  doOwnerSelect() {
    this.isOwnerSelect = true;
    this.credentialService.getCredentials().subscribe((res: Credential[]) => {
      this.toastr.success("Ok!");
      this.credentials = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  selectNewCredential(c: Credential) {
    this.workload.credential = Object.assign(c);
    this.isOwnerSelect = false;
  }

  doVolumeSelect() {
    this.isVolumeSelect = true;
    if(!!this.workload.volume){
      this.selectedVolumes = Object.assign(this.workload.volume);
    }
    this.volumeService.getVolumes().subscribe((res: Volume[]) => {
      this.toastr.success("Ok!");
      this.volumes = res;
      this.volumes.forEach((v: Volume) => {
        let i = this.selectedVolumes.findIndex((sv) => {
          return sv.id === v.id
        });
        if (i > 0) {
          this.volumes.splice(i, 1);
        }
      });
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  selectNewVolume(v: Volume) {
    this.selectedVolumes.push(Object.assign(v));
    let i = this.volumes.findIndex((volume) => {
      return v.id === volume.id;
    });
    this.volumes.splice(i, 1);
  }

  deselectVolume(v: Volume) {
    this.volumes.push(Object.assign(v));
    let i = this.selectedVolumes.findIndex((volume) => {
      return v.id === volume.id;
    });
    this.selectedVolumes.splice(i, 1);
  }

  applyNewVolumes() {
    this.workload.volume = Object.assign(this.selectedVolumes);
    this.isVolumeSelect = false;
  }
}
