import { Component, OnInit } from '@angular/core';
import { Volume } from 'src/app/models/volume';
import { VolumesService } from 'src/app/services/volumes/volumes.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-volumes',
  templateUrl: './volumes.component.html',
  styleUrls: ['./volumes.component.scss']
})
export class VolumesComponent implements OnInit {
  public isEditing: Boolean = false;
  public isAdding: Boolean = false;

  public volumes: Array<Volume> = [];
  public volume: Volume;

  constructor(private service: VolumesService, private toastr: ToastrService) {
    this.fetch();
  }

  ngOnInit(): void {
  }

  fetch() {
    this.service.getVolumes().subscribe((res: Volume[]) => {
      this.toastr.success("Ok!");
      this.volumes = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  edit(c: Volume) {
    this.volume = Object.assign(c);
    this.isEditing = true;
  }

  add() {
    this.volume = new Volume();
    this.isAdding = true;
  }

  save() {
    this.service.addVolume(this.volume).subscribe((res: any) => {
      this.toastr.success("Save Ok!");
      this.fetch();
      this.isAdding = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  update() {
    this.service.updateVolume(this.volume).subscribe((res: any) => {
      this.toastr.success("Update Ok!");
      this.fetch();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });

  }

  delete() {
    this.service.deleteVolume(this.volume.id).subscribe((res: any) => {
      this.toastr.success("Delete Ok!");
      this.fetch();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }
  goBack(){
    this.fetch();
    this.isAdding=false;
    this.isEditing=false
  }

}
