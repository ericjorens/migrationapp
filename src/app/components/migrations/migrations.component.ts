import { Component, OnInit } from '@angular/core';
import { Migration } from 'src/app/models/migration';
import { MigrationsService } from 'src/app/services/migrations/migrations.service';
import { VolumesService } from 'src/app/services/volumes/volumes.service';
import { TargetCloudsService } from 'src/app/services/target-clouds/target-clouds.service';
import { WorkloadsService } from 'src/app/services/workloads/workloads.service';

import { Workload } from 'src/app/models/workload';
import { Volume } from 'src/app/models/volume';
import { TargetCloud } from 'src/app/models/target-cloud';
import { MigrationState } from 'src/app/types/migration-state';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { MigrationHistory } from 'src/app/models/migration-history';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-migrations',
  templateUrl: './migrations.component.html',
  styleUrls: ['./migrations.component.scss']
})
export class MigrationsComponent implements OnInit {

  public isVolumeSelect: boolean = false;
  public isWorkloadSelect: boolean = false;
  public isTargetCloudSelect: boolean = false;
  public isEditing: boolean = false;
  public isAdding: boolean = false;
  public isShowHistory: boolean = false;

  public migrations: Array<Migration> = [];
  public migration: Migration;
  public migrationStateOptions = [];

  public workloads: Array<Workload> = [];
  public targetClouds: Array<TargetCloud> = [];

  public volumesList: Array<Volume> = [];
  public selectedVolumes: Array<Volume> = [];

  public currentRun: MigrationHistory;
  public currentRunPercentage: number = 0;
  private timerSub: Subscription;
  private readonly MIGRATION_RUNTIME: number = 300000;

  public migrationHistory: Array<MigrationHistory> = [];

  constructor(private volumeService: VolumesService, private targetCloudService: TargetCloudsService, private workloadService: WorkloadsService,
    private service: MigrationsService, private toastr: ToastrService) {
    this.fetch();
  }

  ngOnInit(): void {
    let options = Object.keys(MigrationState);
    this.migrationStateOptions = options.slice(options.length / 2);
  }

  fetch() {
    this.service.getMigrations().subscribe((res: Migration[]) => {
      this.toastr.success("Ok!");
      this.migrations = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  edit(m: Migration) {
    this.migration = Object.assign(m);
    this.isEditing = true;

    //subscribe to determine the percentage complete
    this.getCurrentRun(this.migration);
    if (!!this.currentRun) {
      if (this.showByState('RUNNING')) {
        this.subscribeToProgress();
      }
    } else {
      this.currentRunPercentage = 0;
    }

  }

  subscribeToProgress() {
    this.timerSub = interval(10000).subscribe(x => {
      if (!!this.currentRun) {
        this.getCurrentRunPercentage(this.currentRun);
      }
    });
  }

  add() {
    this.migration = new Migration();
    this.selectedVolumes = Object.assign([]);
    this.volumesList = Object.assign([]);
    this.isAdding = true;
  }

  save() {
    if (this.validateForm()) {
      this.service.addMigration(this.migration).subscribe((res: any) => {
        this.fetch();
        this.isAdding = false;
        this.toastr.success("Save Ok!");
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.message);
      });
    }
  }

  update() {
    if (this.validateForm()) {
      this.service.updateMigration(this.migration).subscribe((res: any) => {
        this.toastr.success("Update Ok!");
        this.fetch();
        this.isEditing = false;
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.message);
      });
    }

  }

  validateForm() {
    if (!this.migration.volume || this.migration.volume.length < 1) {
      this.toastr.info("No migration volume selected!");
      return false;
    } else if (!this.migration.workload) {
      this.toastr.info("No workload selected!");
      return false;
    } else if (!this.migration.targetCloud) {
      this.toastr.info("No target cloud selected!");
      return false;
    }

    return true;
  }

  delete() {
    this.service.deleteMigration(this.migration.id).subscribe((res: any) => {
      this.toastr.success("Delete Ok!");
      this.fetch();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  getVolumes(volumeArray: Array<Volume>): string {
    if (!!volumeArray) {
      let vols: string = '';
      volumeArray.forEach((v: Volume) => {
        vols += '[' + v.mountPoint + ']    ';
      });
      return vols;
    }
    return '';
  }

  goBack() {
    if (!!this.timerSub) {
      this.timerSub.unsubscribe();
    }
    this.fetch();
    this.isAdding = false;
    this.isEditing = false
  }

  doWorkloadSelect() {
    this.isWorkloadSelect = true;
    this.workloadService.getWorkloads().subscribe((res: Workload[]) => {
      this.toastr.success("Ok!");
      this.workloads = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  selectNewWorkload(w: Workload) {
    this.migration.workload = Object.assign(w);
    this.migration.volume = Object.assign([]);
    this.isWorkloadSelect = false;
  }

  doTargetCloudSelect() {
    this.isTargetCloudSelect = true;
    this.targetCloudService.getTargetClouds().subscribe((res: TargetCloud[]) => {
      this.toastr.success("Ok!");
      this.targetClouds = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  selectNewTargetCloud(t: TargetCloud) {
    this.migration.targetCloud = Object.assign(t);
    this.isTargetCloudSelect = false;
  }


  doVolumeSelect() {
    if (!!this.migration && !!this.migration.volume) {
      this.isVolumeSelect = true;
      this.selectedVolumes = Object.assign(this.migration.volume);

      //This is needed to break obj refs
      this.volumesList = [];
      this.migration.workload.volume.forEach((v: Volume) => {
        this.volumesList.push(Object.assign(v));
      });

      //now filter the lists
      //want only the volumes that apply to the workload
      this.selectedVolumes = this.selectedVolumes.filter((svl: Volume) => {
        return this.volumesList.findIndex((vl: Volume) => { return vl.id == svl.id }) > -1;
      });
      //now filter the available volumes to add
      this.volumesList = this.volumesList.filter((vol: Volume) => {
        return this.selectedVolumes.findIndex((sv: Volume) => { return sv.id == vol.id }) < 0;
      });
    }
  }

  selectNewVolume(v: Volume) {
    this.selectedVolumes.push(Object.assign(v));
    let i = this.volumesList.findIndex((volume) => {
      return v.id === volume.id;
    });
    this.volumesList.splice(i, 1);
  }

  deselectVolume(v: Volume) {
    this.volumesList.push(Object.assign(v));
    let i = this.selectedVolumes.findIndex((volume) => {
      return v.id === volume.id;
    });
    this.selectedVolumes.splice(i, 1);
  }

  applyNewVolumes() {
    this.migration.volume = Object.assign(this.selectedVolumes);
    this.isVolumeSelect = false;
  }

  showByState(v: string) {
    if (!!this.migration) {
      return this.migration.state.toString() === v;
    }
    return true;
  }

  showHistory(m: Migration) {
    this.migration = Object.assign(m);
    this.service.getAllMigrationHistory(m.id).subscribe((res: Array<MigrationHistory>) => {
      this.migrationHistory = Object.assign(res);
      this.getCurrentRun(m);
      this.isEditing = true;
      this.isShowHistory = true;
      this.toastr.success("Ok!");
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  getCurrentRun(m: Migration) {
    this.service.getMigrationHistory(m.id).subscribe((res: MigrationHistory) => {
      this.currentRun = Object.assign(res);
      this.getCurrentRunPercentage(this.currentRun);
    }, (err: HttpErrorResponse) => {
      this.toastr.info("This migration has not been run!");
      this.currentRunPercentage = 0;
    });
  }

  getCurrentRunPercentage(mh: MigrationHistory) {
    this.service.getMigrationHistoryPercentage(mh).subscribe((res: number) => {
      this.currentRunPercentage = Math.round((res / this.MIGRATION_RUNTIME) * 100);
      if (this.currentRunPercentage > 100) {
        this.currentRunPercentage = 100;
        if (!!this.timerSub) {
          this.timerSub.unsubscribe();
        }
        this.fetch();
        this.refreshMigration(this.migration);
      }
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  getProgressValue() {
    return this.currentRunPercentage.toString() + '%';
  }

  stopMigration(m: Migration) {
    this.service.getMigrationHistory(m.id).subscribe((mhres: MigrationHistory) => {
      let mh = Object.assign(mhres);
      this.service.stopMigration(mh.id).subscribe((mres: MigrationHistory) => {
        this.currentRun = Object.assign(mres);
        this.getCurrentRunPercentage(this.currentRun);
        this.fetch();
        this.refreshMigration(m);
        this.toastr.info("Migration Stopped!");
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.message);
      });
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  startMigration(m: Migration) {
    this.service.startMigration(m.id).subscribe((res: MigrationHistory) => {
      this.currentRun = Object.assign(res);
      this.getCurrentRunPercentage(this.currentRun);
      this.fetch();
      this.refreshMigration(m);
      this.subscribeToProgress();
      this.toastr.info("Migration Started!");
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  refreshMigration(m: Migration) {
    this.service.getMigration(m.id).subscribe((mr: Migration) => {
      this.migration = Object.assign(mr);
      this.service.getAllMigrationHistory(this.migration.id).subscribe((res: Array<MigrationHistory>) => {
        this.migrationHistory = Object.assign(res);
      }, (err: HttpErrorResponse) => {
        this.toastr.error(err.error.message);
      });
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

}
