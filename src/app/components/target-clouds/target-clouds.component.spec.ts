import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetCloudsComponent } from './target-clouds.component';

describe('TargetCloudsComponent', () => {
  let component: TargetCloudsComponent;
  let fixture: ComponentFixture<TargetCloudsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TargetCloudsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetCloudsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
