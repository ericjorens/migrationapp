import { Component, OnInit } from '@angular/core';
import { TargetCloud } from 'src/app/models/target-cloud';
import { TargetCloudsService } from 'src/app/services/target-clouds/target-clouds.service';
import { Credential } from '../../models/credential';
import { Workload } from 'src/app/models/workload';
import { Volume } from 'src/app/models/volume';
import { CloudType } from 'src/app/types/cloud-type';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';
import { CredentialsService } from 'src/app/services/credentials/credentials.service';
import { WorkloadsService } from 'src/app/services/workloads/workloads.service';

@Component({
  selector: 'app-target-clouds',
  templateUrl: './target-clouds.component.html',
  styleUrls: ['./target-clouds.component.scss']
})
export class TargetCloudsComponent implements OnInit {

  public isEditing: Boolean = false;
  public isAdding: Boolean = false;
  public isOwnerSelect: Boolean = false;
  public isWorkloadSelect: boolean = false;

  public targetClouds: Array<TargetCloud> = [];
  public targetCloud: TargetCloud;
  public credential: Credential;
  public workload: Workload;

  private cloudTypes = CloudType;
  public cloudTypeOptions = [];

  public credentials: Array<Credential> = [];
  public workloads: Array<Workload> = [];

  public volumes: Array<Volume> = [];
  public selectedVolumes: Array<Volume> = [];


  constructor(private service: TargetCloudsService, private toastr: ToastrService,
    private credentialService: CredentialsService, private workloadService: WorkloadsService) {
    this.fetchTargetClouds();
    let options = Object.keys(this.cloudTypes);
    this.cloudTypeOptions = options.slice(options.length / 2);
  }

  ngOnInit(): void {
    let options = Object.keys(this.cloudTypes);
    this.cloudTypeOptions = options.slice(options.length / 2);
  }

  fetchTargetClouds() {
    this.service.getTargetClouds().subscribe((res: TargetCloud[]) => {
      this.toastr.success("Ok!");
      this.targetClouds = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  edit(c: TargetCloud) {
    this.targetCloud = Object.assign(c);
    this.isEditing = true;
  }

  add() {
    this.targetCloud = new TargetCloud();
    this.isAdding = true;
  }

  save() {
    this.service.addTargetCloud(this.targetCloud).subscribe((res: any) => {
      this.toastr.success("Save Ok!");
      this.fetchTargetClouds();
      this.isAdding = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  doOwnerSelect() {
    this.isOwnerSelect = true;
    this.credentialService.getCredentials().subscribe((res: Credential[]) => {
      this.toastr.success("Ok!");
      this.credentials = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  selectNewCredential(c: Credential) {
    this.targetCloud.credential = Object.assign(c);
    this.credential = Object.assign(c);
    this.isOwnerSelect = false;
  }

  doWorkloadSelect(){
    this.isWorkloadSelect = true;
    this.workloadService.getWorkloads().subscribe((res: Workload[]) => {
      this.toastr.success("Ok!");
      this.workloads = res;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  selectNewWorkload(w: Workload) {
    this.targetCloud.workload = Object.assign(w);
    this.workload = Object.assign(w);
    this.isWorkloadSelect = false;
  }

  update() {
    this.service.updateTargetCloud(this.targetCloud).subscribe((res: any) => {
      this.toastr.success("Update Ok!");
      this.fetchTargetClouds();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });

  }

  delete() {
    this.service.deleteTargetCloud(this.targetCloud.id).subscribe((res: any) => {
      this.toastr.success("Delete Ok!");
      this.fetchTargetClouds();
      this.isEditing = false;
    }, (err: HttpErrorResponse) => {
      this.toastr.error(err.error.message);
    });
  }

  getVolumes(w: Workload): string {
    if (!!w) {
      let vols: string = '';
      w.volume.forEach((v: Volume) => {
        vols += '[' + v.mountPoint + ']    ';
      });
      return vols;
    }
    return '';
  }

  goBack() {
    this.fetchTargetClouds();
    this.isAdding = false;
    this.isEditing = false
  }
}
