/* @author Eric Jorens */

import { Volume } from './volume';
import { Credential } from './credential';

export class Workload{
    id: number;
    ip : string;
    credential:Credential;
    volume: Array<Volume>;

    constructor(){}
}