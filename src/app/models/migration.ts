/* @author Eric Jorens */

import { Volume } from './volume';
import { Workload } from './workload';
import { TargetCloud } from './target-cloud';
import { MigrationState } from '../types/migration-state';

export class Migration{
    id: number;
    volume: Array<Volume>;
    workload: Workload;
    targetCloud: TargetCloud;
    state: MigrationState = MigrationState.NOT_STARTED;

    constructor(){}
}