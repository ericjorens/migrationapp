import { Migration } from './migration';

export class MigrationHistory{
    id: number;
    migration: Migration;
    start: Date;
    end: Date;
    message:string;
    
    constructor(){}
}