/* @author Eric Jorens */

import { Workload } from './workload';
import { CloudType } from '../types/cloud-type';
import { Credential } from './credential';

export class TargetCloud{
    id: number;
    cloudType:CloudType = CloudType.AWS;
    credential:Credential;
    workload:Workload;

    constructor(){}
}