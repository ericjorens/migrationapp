import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CredentialsComponent } from './components/credentials/credentials.component';
import { VolumesComponent } from './components/volumes/volumes.component';
import { MigrationsComponent } from './components/migrations/migrations.component';
import { TargetCloudsComponent } from './components/target-clouds/target-clouds.component';
import { WorkloadsComponent } from './components/workloads/workloads.component';


const routes: Routes = [
  { path: '', redirectTo: 'credentials', pathMatch:'full' },
  { path: 'credentials', component: CredentialsComponent },
  { path: 'volumes', component: VolumesComponent },
  { path: 'migrations', component: MigrationsComponent },
  { path: 'target-clouds', component: TargetCloudsComponent },
  { path: 'workloads', component: WorkloadsComponent },
  { path: '**', redirectTo: '' },
  { path: '**/**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
