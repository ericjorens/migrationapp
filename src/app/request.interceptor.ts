import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let cloneReq = req.clone();
    cloneReq.headers.set(
      "Content-Type", "application/json",
    );
    cloneReq.headers.set(
      "Access-Control-Allow-Origin", "*",
    );

    return next.handle(cloneReq);
  }
}