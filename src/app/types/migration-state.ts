export enum MigrationState {
    NOT_STARTED = 0,
    RUNNING = 1,
    ERROR = 2,
    SUCCESS = 3
}