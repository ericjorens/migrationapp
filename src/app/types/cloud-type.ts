export enum CloudType {
    AWS,
    AZURE,
    VSPHERE,
    VCLOUD
}